HELM : The Package Manager for Kubernetes


Difference between Helm v2 and v3
    v2 uses tiller as a server component to deploy charts
    v3 uses RBAC to deploy charts  

HELM Hub Repo :
https://artifacthub.io/#

HELM VERSION :
#helm version

SEARCH ALL CHARTS AVAILABLE ON HUB :
#helm search hub

SEARCH JENKINS CHART AVAIABLES ON HUB :
#helm search hub jenkins

ADD THE REPOSITORY TO LOCAL MACHINE :
#helm repo add <repo-name> <repo-url>
i.e. :  helm repo add stable https://charts.helm.sh/stable 
Ref URL : https://helm.sh/blog/new-location-stable-incubator-charts/

UPDATE REPO WITH LATEST:
#helm repo update

LIST Local REPO ADDED:
#helm repo list

SERACH STABLE RELEASE IN REPO:
helm search repo stable

SERACH STABLE MYSQL RELEASE IN REPO:
helm search repo stable/mysql

HELM CHART INSTALLATION :
DOWNLOAD A CHART TO YOUR LOCAL MACHINE TO VIEW :
#helm pull stable/airflow

INSTALL CHART TO KUBERNETES :
#helm install mysql stable/mysql

UNINSTALL CHART TO KUBERNETES :
#helm uninstall mysql

LIST RELEASES OF CHARTS :
#helm list

CREATE A NEW CHART :
#helm create mychart

INSTALL CHART ON K8S CLUSTER :
#helm install mychart ./mychart

Built-in Objects in Helm :
Ref URL : https://helm.sh/docs/chart_template_guide/builtin_objects/

ONCE THE DEPLOYMENT DONE, TO CHECK THE TEMPLATE VALUE REPLACEMENT RUN BELOW COMMAND:
#helm get manifest <helm-release-name>

WITHOUT DEPLOYMENT HOW WE CAN CHECK THE REPLACEMENT OF TEMPLATE VALUES:
#helm install --debug --dry-run firstdryrun ./mychart 

WITHOUT DEPLOYMENT HOW WE CAN CHECK THE REPLACEMENT OF TEMPLATE VALUES USING CLI:
helm install --debug --dry-run --set Name="Swati Chordia" sethelmvar ./mychart

TEMPLATE FUNCTION :
Ref URL : http://masterminds.github.io/sprig/

-----------------------------------------------------------------------------
Flow Control If-else

{{ if PIPELINE }}
    # Do Something
{{ else if OTHER PIPELINE }}
    # Do Something else
{{ else }}
    # Default Case
{{ end }}


A pipeline is evaluated as false if the value is :
    a Boolean false
    a numeric zero
    an empty string
    a nil (empty or null)
    an empty collection (map, slice, tuple, dict, array)



-----------------------------------------------------------------------------

